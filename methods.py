#!/usr/bin/env python

import serial
import json

def getPoidsBalance() :
    try:
        ser = serial.Serial(port='/dev/ttyUSB0', baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE,
                            bytesize=serial.EIGHTBITS, timeout=1)
        ligne = ser.readline().strip()
        tab = ligne.decode().split()
        global ancien_poids
        #print(len(tab))
        if len(tab) >= 3:
            poids = tab[1]
            if poids == "-":
                return 0 - int(tab[2])
            ancien_poids = int(poids)
            return int(poids)
        else:
            return ancien_poids
    except:
        pass
        #print("no balance or USB connection...")
        return 0


def getConfig():
    global POIDS_ZERO
    global POIDS_MIN_CANARD
    global POIDS_MAX_CANARD
    global CONFIG_FILE

    try:
        with open(CONFIG_FILE, "r") as conf:
            data = json.load(conf)
            POIDS_ZERO = data["TARE"]
            POIDS_MIN_CANARD = data["POIDS_MIN"]
            POIDS_MAX_CANARD = data["POIDS_MAX"]
    except:
        POIDS_ZERO = None
        POIDS_MIN_CANARD = None
        POIDS_MAX_CANARD = None

        # print("POIDS_ZERO : " + str(POIDS_ZERO))
        # print("POIDS_MIN_CANARD : " + str(POIDS_MIN_CANARD))
        # print("POIDS_MAX_CANARD : " + str(POIDS_MAX_CANARD))


def setTare(pds_brut):
    global TARE
    if pds_brut == 0:
        # print("TARE = "+ str(TARE) +" brut = " + str(poids_brut) + " POIDS0 = " + str(POIDS_ZERO))
        TARE = 0
    else:
        if pds_brut - TARE < POIDS_ZERO:
            TARE = pds_brut


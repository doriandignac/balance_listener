#!/usr/bin/env python
# coding: utf-8
import requests
import methods
import statistics
import systemd_stopper
import serial
import json

# CONSTANTES EN PARAMETRE ou NON
# Poids en Gramme

#Poids reinitialisé dès le début !
POIDS_ZERO = 0  # int(sys.argv[1])#100
POIDS_MIN_CANARD = 0  # int(sys.argv[2])#400
POIDS_MAX_CANARD = 0  # int(sys.argv[3])#750
CONFIG_FILE = "/opt/pese_canards/config_pesee.json"

# GLOBALES
TARE = 0

def getPoidsBalance() :
    try:
        ser = serial.Serial(port='/dev/ttyUSB0', baudrate=9600, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE,
                            bytesize=serial.EIGHTBITS, timeout=1)
        ligne = ser.readline().strip()
        tab = ligne.decode().split()
        global ancien_poids
        #print(len(tab))
        if len(tab) >= 3:
            poids = tab[1]
            if poids == "-":
                return 0 - int(tab[2])
            ancien_poids = int(poids)
            return int(poids)
        else:
            return ancien_poids
    except:
        pass
        #print("no balance or USB connection...")
        return 0


def getConfig():
    global POIDS_ZERO
    global POIDS_MIN_CANARD
    global POIDS_MAX_CANARD
    global CONFIG_FILE

    try:
        with open(CONFIG_FILE, "r") as conf:
            data = json.load(conf)
            POIDS_ZERO = data["TARE"]
            POIDS_MIN_CANARD = data["POIDS_MIN"]
            POIDS_MAX_CANARD = data["POIDS_MAX"]
    except:
        POIDS_ZERO = None
        POIDS_MIN_CANARD = None
        POIDS_MAX_CANARD = None

        # print("POIDS_ZERO : " + str(POIDS_ZERO))
        # print("POIDS_MIN_CANARD : " + str(POIDS_MIN_CANARD))
        # print("POIDS_MAX_CANARD : " + str(POIDS_MAX_CANARD))


def setTare(pds_brut):
    global TARE
    if pds_brut == 0:
        # print("TARE = "+ str(TARE) +" brut = " + str(poids_brut) + " POIDS0 = " + str(POIDS_ZERO))
        TARE = 0
    else:
        if pds_brut - TARE < POIDS_ZERO:
            TARE = pds_brut




if __name__ == '__main__':

    stopper = systemd_stopper.install()

    setTare(methods.getPoidsBalance())

    while stopper.run:
        liste_poids = []
        getConfig()

        while stopper.run:

            # si le fichier n'est pas présent
            if POIDS_ZERO == None and POIDS_MIN_CANARD == None and POIDS_MAX_CANARD == None:
                #print("Pas de fichier")
                break

            # calcul du poids
            poids_brut = methods.getPoidsBalance()
            poids_net = poids_brut - TARE

            # test si la balance est vide
            if poids_brut <= POIDS_ZERO:
                setTare(poids_brut)
                getConfig()
                # print("Poids 0, Tare = " + str(TARE))#####################################
                # print("Pbrut : " + str(poids_brut) + " Pnet : " + str(poids_net) + " tare : " + str(TARE))#########################
                break

            else:
                # teste l'intervalle de poids des canards
                if poids_net < POIDS_MIN_CANARD or poids_net > POIDS_MAX_CANARD:
                    # print("Poids sup ou inf")#####################################
                    # print("Pbrut : " + str(poids_brut) + " Pnet : " + str(poids_net) + " tare : " + str(TARE))#########################
                    break

                # ajoute le poids au tableau s'il n'y est pas
                # print ("Poids valide : " + str(poids_net))#####################################
                # print("Pbrut : " + str(poids_brut) + " Pnet : " + str(poids_net) + " tare : " + str(TARE))#########################
                if poids_net not in liste_poids:
                    liste_poids.append(poids_net)

        if len(liste_poids) > 0:
            poids_choisi = int(statistics.median(liste_poids))
            print(liste_poids)
            print("Canard ajouté : " + str(poids_choisi))

            headers = {'content-type': 'application/json'}
            payload = {"poids": poids_choisi}

            try:
                requests.post('http://localhost:8000/api/poids', json=payload, headers=headers)

            except:
                pass
            liste_poids.clear()
    exit(0)

